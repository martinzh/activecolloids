#!/bin/bash

# Script para enviar simulaciones a:
# temperatura T, relacion de aspecto R
# barriendo las Fsp dadas en el for

T=$1 # Temperatura
R=$2 # Aspect ratio

for f in 0 2.5 5 7.5 10 12.5 15 17.5 20
    do
        sbatch sim_chan.sh $f $R $T
done
