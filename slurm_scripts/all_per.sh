#!/bin/bash

# Script para mandar simulaciones a temperatura T
# barriendo Fsp con los valores en el for

T=$1

for f in 0 2.5 5 7.5 10 12.5 15 17.5 20
    do
        sbatch sim_per.sh $f $T
done
