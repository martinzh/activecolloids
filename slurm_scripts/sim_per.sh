#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --output=output/output_%A_%a.log
#SBATCH --error=error/error_%A_%a.log
#SBATCH --job-name=sim_per
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=martinzh@icf.unam.mx
#SBATCH --array=10-20
#SBATCH --partition=prod

F=$1 # Fuerza de autopropulsion
M=$2 # Temperatura

scl enable devtoolset-6 bash

~/GitRepos/selfAssembly/src/deploy/./SA_per.x 5 512 135 $F 250 $M 0.001 7 $SLURM_ARRAY_TASK_ID
