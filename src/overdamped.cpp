#ifndef _OVERDAMPED_CPP_
#define _OVERDAMPED_CPP_

#include <cstdlib>
#include <iostream>
#include <math.h>

using std::cout;
using std::endl;
using std::cerr;

#include "mdsys.h"
#include "Utils.h"
#include "LJForce.h"
#include "overdamped.h"

//////////////////////////////////////////////////

Overdamped::Overdamped(std::shared_ptr<MDSystem> umdsys, int rep) {
    mds=umdsys;
    f_prop     = mds->f_prop;
    t_visc     = mds->t_visc;
    n_visc     = mds->n_visc;
    ang_n_visc = mds->ang_n_visc;

    rng = mt19937(rd());
    rng.seed(time(NULL)+rep);

    dist = normal_distribution<double>(0.0, 1.0);

    // first_int = true;
}

//////////////////////////////////////////////////

void Overdamped::check_bound(){

    vector<Boid*> boid = mds->boid;

    int nsegs      = mds->boid[0]->segs.size();
    int nverts     = mds->boid[0]->verts.size();
    int N_colloids = mds->N_colloids;

    double box_length_x = mds->box_length_x;
    double box_length_y = mds->box_length_y;

    MVector n_pos;

    //////////////////////////////////////////////////

    for(int i = 0; i < N_colloids; i ++){

        n_pos.set(boid[i]->pos);

        if(n_pos.x <= 0){
            n_pos.x += box_length_x;
        }else if(n_pos.x >= box_length_x){
            n_pos.x -= box_length_x;
        }

        if(n_pos.y <= 0){
            n_pos.y += box_length_y;
        }else if(n_pos.y >= box_length_y){
            n_pos.y -= box_length_y;
        }

        for(int j = 0; j < nsegs; j++){
            boid[i]->segs[j].x += n_pos.x - boid[i]->pos.x;
            boid[i]->segs[j].y += n_pos.y - boid[i]->pos.y;
        }
        for(int j = 0; j < nverts; j++){
            boid[i]->verts[j].x += n_pos.x - boid[i]->pos.x;
            boid[i]->verts[j].y += n_pos.y - boid[i]->pos.y;
        }

        boid[i]->pos.set(n_pos);
    }
}

//////////////////////////////////////////////////

void Overdamped::check_bound_channel(){

    vector<Boid*> boid = mds->boid;

    int nsegs      = mds->boid[0]->segs.size();
    int nverts     = mds->boid[0]->verts.size();
    int N_colloids = mds->N_colloids;

    double box_length_y = mds->box_length_y;

    MVector n_pos;

    //////////////////////////////////////////////////

    for(int i = 0; i < N_colloids; i ++){

        n_pos.set(boid[i]->pos);

        if(n_pos.y <= 0){
            n_pos.y += box_length_y;
        }else if(n_pos.y >= box_length_y){
            n_pos.y -= box_length_y;
        }

        for(int j = 0; j < nsegs; j++){
            // boid[i]->segs[j].x += n_pos.x - boid[i]->pos.x;
            boid[i]->segs[j].y += n_pos.y - boid[i]->pos.y;
        }
        for(int j = 0; j < nverts; j++){
            // boid[i]->verts[j].x += n_pos.x - boid[i]->pos.x;
            boid[i]->verts[j].y += n_pos.y - boid[i]->pos.y;
        }

        boid[i]->pos.set(n_pos);
    }
}

//////////////////////////////////////////////////

void Overdamped::set_ghosts(){

    vector<Boid*> boid = mds->boid;
    vector<Boid*> gh_boid = mds->gh_boid;

    bool* gh_flag  = mds->gh_flag;

    double box_length = mds->box_length;
    double thr = mds->thrd;

    int N = mds->N_colloids;

    MVector n_pos;

    //////////////////////////////////////////////////

    for(int i = 0; i < N; i ++){

        gh_flag[i]     = false;
        gh_flag[i+N]   = false;
        gh_flag[i+2*N] = false;

        gh_boid[i]->set(boid[i]);
        gh_boid[i+N]->set(boid[i]);
        gh_boid[i+2*N]->set(boid[i]);

        //////////////////////////////////////////////////
        if(boid[i]->pos.x <= thr || boid[i]->pos.x >= box_length - thr ||
            boid[i]->pos.y <= thr || boid[i]->pos.y >= box_length - thr){

            if(boid[i]->pos.x >= box_length - thr && boid[i]->pos.y >= box_length - thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x -= box_length;
                n_pos.y -= box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x -= box_length;
                gh_boid[i+N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+2*N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.y -= box_length;
                gh_boid[i+2*N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x <= thr && boid[i]->pos.y <= thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x += box_length;
                n_pos.y += box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x += box_length;
                gh_boid[i+N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+2*N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.y += box_length;
                gh_boid[i+2*N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x <= thr && boid[i]->pos.y >= box_length - thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x += box_length;
                n_pos.y -= box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x += box_length;
                gh_boid[i+N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+2*N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.y -= box_length;
                gh_boid[i+2*N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x >= box_length - thr && boid[i]->pos.y <= thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x -= box_length;
                n_pos.y += box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x -= box_length;
                gh_boid[i+N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
                gh_flag[i+2*N] = true;
                n_pos.set(boid[i]->pos);
                n_pos.y += box_length;
                gh_boid[i+2*N]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x >= box_length - thr && boid[i]->pos.y < box_length - thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x -= box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x < box_length - thr && boid[i]->pos.y >= box_length - thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.y -= box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x <= thr && boid[i]->pos.y > thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.x += box_length;
                gh_boid[i]->translate_boid(n_pos);
                //////////////////////////////////////////////////
            }else if(boid[i]->pos.x > thr && boid[i]->pos.y <= thr){
                gh_flag[i] = true;
                n_pos.set(boid[i]->pos);
                n_pos.y += box_length;
                gh_boid[i]->translate_boid(n_pos);
            }
            //////////////////////////////////////////////////
        }
    }
}

//////////////////////////////////////////////////

void Overdamped::set_ghosts_channel(){

    vector<Boid*> boid = mds->boid;
    vector<Boid*> gh_boid = mds->gh_boid;

    bool* gh_flag  = mds->gh_flag;

    int N = mds->N_colloids;

    double thr = mds->thrd;
    double box_length_y = mds->box_length_y;

    MVector n_pos;

    //////////////////////////////////////////////////

    for(int i = 0; i < N; i ++){

        gh_flag[i]     = false;
        gh_boid[i]->set(boid[i]);

        //////////////////////////////////////////////////
        if(boid[i]->pos.y >= box_length_y - thr){
            gh_flag[i] = true;
            n_pos.set(boid[i]->pos);
            n_pos.y -= box_length_y;
            gh_boid[i]->translate_boid(n_pos);
            //////////////////////////////////////////////////
        }else if(boid[i]->pos.y <= thr){
            gh_flag[i] = true;
            n_pos.set(boid[i]->pos);
            n_pos.y += box_length_y;
            gh_boid[i]->translate_boid(n_pos);
        }
        //////////////////////////////////////////////////
    }
}

//////////////////////////////////////////////////

void Overdamped::update_verlet_pos(){

    int N_colloids = mds->N_colloids;

    int nsegs  = mds->boid[0]->segs.size();
    int nverts = mds->boid[0]->verts.size();

    double b_mass    = mds->boid[0]->b_mass;
    double inertia_m = mds->boid[0]->inertia_m;

    //////////////////////////////////////////////////

    double dt = mds->dt;
    double n_visc = mds->n_visc;
    // double t_visc = mds->t_visc;
    // double ang_n_visc = mds->ang_n_visc;

    double b_verl = mds->b_verl;
    // double f_prop = mds->f_prop;

    //////////////////////////////////////////////////

    vector<Boid*> boid = mds->boid;

    vector<MVector*> force = mds->force;
    double* torque = mds->torque;

    // vector<MVector*> prev_force = mds->prev_force;
    // double* prev_torque = mds->prev_torque;

    //////////////////////////////////////////////////

    MVector n_pos;
    double n_ang;

    double delta_ang;

    double TWO_PI   = 2.0 * M_PI;
    double M_TWO_PI = -2.0 * M_PI;

    // bool first_int = mds->first_int;

    //////////////////////////////////////////////////

    // mds->first_int = true;

    for (int i = 0; i < N_colloids; i++) {

        boid[i]->x_n = dist(rng);
        boid[i]->y_n = dist(rng);

        boid[i]->x_n_a = dist(rng);
        boid[i]->y_n_a = dist(rng);

        // if(force[i]->x > 0.0 || force[i]->y > 0.0) printf("%f,%f\n", force[i]->x, force[i]->y);

        n_pos.x = boid[i]->pos.x + b_verl * dt * boid[i]->vel.x +
            ((0.5 * b_verl * dt * dt)/b_mass) * force[i]->x + (0.5 * b_verl * dt *n_visc * boid[i]->x_n)/b_mass;

        n_pos.y = boid[i]->pos.y + b_verl * dt * boid[i]->vel.y +
            ((0.5 * b_verl * dt * dt)/b_mass) * force[i]->y + (0.5 * b_verl * dt *n_visc * boid[i]->y_n)/b_mass;

        // n_ang = boid[i]->ang + dt*b_verl*boid[i]->w_ang + (0.5*b_verl*dt*dt*torque[i])/inertia_m + (0.5*dt*b_verl*n_visc * (boid[i]->x_n * sin(boid[i]->ang) - boid[i]->y_n * cos(boid[i]->ang)))/inertia_m;
        n_ang = boid[i]->ang + b_verl * dt * boid[i]->w_ang +
            (0.5 * b_verl * dt * dt)/inertia_m * torque[i] +
            (0.5 * b_verl * dt * n_visc) * (boid[i]->x_n_a * sin(boid[i]->ang) -
            boid[i]->y_n_a * cos(boid[i]->ang)) /inertia_m;

        //////////////////////////////////////////////////

        // if(n_ang > TWO_PI)   n_ang -= TWO_PI;
        // if(n_ang < M_TWO_PI) n_ang += TWO_PI;

        delta_ang = n_ang - boid[i]->ang;

        if(delta_ang > TWO_PI)   delta_ang -= TWO_PI;
        if(delta_ang < M_TWO_PI) delta_ang += TWO_PI;

        //////////////////////////////////////////////////

        for(int j = 0; j < nsegs; j ++){
            boid[i]->segs[j].rotateRad(delta_ang, boid[i]->pos);

            boid[i]->segs[j].x +=  n_pos.x - boid[i]->pos.x;
            boid[i]->segs[j].y +=  n_pos.y - boid[i]->pos.y;
        }

        //////////////////////////////////////////////////

        for(int j = 0; j < nverts; j ++){
            boid[i]->verts[j].rotateRad(delta_ang, boid[i]->pos);

            boid[i]->verts[j].x +=  n_pos.x - boid[i]->pos.x;
            boid[i]->verts[j].y +=  n_pos.y - boid[i]->pos.y;
        }

        //////////////////////////////////////////////////

        boid[i]->prev_pos.x = boid[i]->pos.x;
        boid[i]->prev_pos.y = boid[i]->pos.y;
        boid[i]->prev_ang = boid[i]->ang;

        boid[i]->pos.x = n_pos.x;
        boid[i]->pos.y = n_pos.y;
        boid[i]->ang = n_ang;
    }
}

//////////////////////////////////////////////////

void Overdamped::update_verlet_vel(){

    int N_colloids = mds->N_colloids;

    double b_mass    = mds->boid[0]->b_mass;
    double inertia_m = mds->boid[0]->inertia_m;

    //////////////////////////////////////////////////

    double dt = mds->dt;
    double visc = mds->visc;
    double n_visc = mds->n_visc;
    // double f_prop = mds->f_prop;

    //////////////////////////////////////////////////

    vector<Boid*> boid = mds->boid;

    vector<MVector*> force = mds->force;
    vector<MVector*> prev_force = mds->prev_force;

    double* torque = mds->torque;
    double* prev_torque = mds->prev_torque;

    //////////////////////////////////////////////////
    MVector n_vel;
    double n_w_ang;
    // bool first_int = mds->first_int;

    //////////////////////////////////////////////////

    // mds->first_int = false;

    for (int i = 0; i < N_colloids; i++) {

        // if(force[i]->x > 0.0 || force[i]->y > 0.0 || prev_force[i]->x > 0.0 || prev_force[i]->y > 0.0) printf("%f,%f,%f,%f\n", force[i]->x, force[i]->y, prev_force[i]->x, prev_force[i]->y);

        n_vel.x = boid[i]->vel.x + (0.5*dt/b_mass) * (prev_force[i]->x + force[i]->x) -
            (visc/b_mass) * (boid[i]->pos.x - boid[i]->prev_pos.x) + (1.0/b_mass) * n_visc * boid[i]->x_n;
        n_vel.y = boid[i]->vel.y + (0.5*dt/b_mass) * (prev_force[i]->y + force[i]->y) -
            (visc/b_mass) * (boid[i]->pos.y - boid[i]->prev_pos.y) + (1.0/b_mass) * n_visc * boid[i]->y_n;

        // n_w_ang = boid[i]->w_ang + (0.5*dt/inertia_m) * (prev_torque[i] + torque[i]) - (visc/inertia_m) * (boid[i]->ang - boid[i]->prev_ang) + (1.0/inertia_m) * n_visc * (x_n * sin(boid[i]->ang) - y_n * cos(boid[i]->ang));
        n_w_ang = boid[i]->w_ang + (0.5*dt/inertia_m) * (prev_torque[i] + torque[i]) - (visc/inertia_m)
            * (boid[i]->ang - boid[i]->prev_ang) + (1.0/inertia_m)
            * n_visc * (boid[i]->x_n_a * sin(boid[i]->ang) - boid[i]->y_n_a * cos(boid[i]->ang));

        //////////////////////////////////////////////////

        boid[i]->vel.x = n_vel.x;
        boid[i]->vel.y = n_vel.y;
        boid[i]->w_ang = n_w_ang;
    }
}

////////////////////////////////////////////////////////////////////////////

#endif
