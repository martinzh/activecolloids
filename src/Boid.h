//
//  Boid.h
//
//  Created by Martin Zumaya on 1/16/18.
//

#ifndef Boid_h
#define Boid_h

#include <stdio.h>
#include <stdlib.h>
#include <vector>

// #include "MVector.h"
// #include "Utils.h"

using namespace std;

class Boid {

    public:

    MVector pos, vel, prev_pos;

    int cell_id [2];
    
    double ang, w_ang, prev_ang;
    double x_n, y_n, x_n_a, y_n_a;

    vector<MVector> segs, verts;
    vector<int> type;

    double b_mass, inertia_m;

    /// BOID INITIALIZATION ///
    void setup(int species, double Lx, double Ly, double tol, double r, double scale, double mass);
    void set_side(int i_vert, int f_vert, int id, double r);
    void set(Boid* boid);
    double compute_inertia_moment(double m);

    /// GEOMETRIC UTILITY FUNCTIONS ///
    double line_point_distance(double a, double b, double c, double x0, double y0);
    MVector line_point_vector(double a, double b, double c, double x0, double y0);

    /// BOID ADJUSTMENT FUNCTIONS ///
    void translate_boid(MVector dir);

    /// COEFFICIENTS AND FORCE CALCULATION ///

    double lennard_jones_force_coef(double epsilon, double r_inv, double r_seg, double rab_2, double r_corr);

    vector<MVector> force_torque_lennard_jones(Boid* boid_b, double att_range, double att_en, double att_corr, double rep_range, double rep_corr, double r_seg);
    vector<MVector> force_torque_lennard_jones_types(Boid* boid_b, double att_range, double att_en, double att_corr, double rep_range, double rep_corr, double r_seg);

    /// BOUNDARIES REPULSION ///
    vector<MVector> force_torque_wall(double a, double b, double c, double rep_range, double rep_corr, double r_seg);

    /// PERIODIC BOUNDARY CONDITIONS ///

    Boid();
    ~Boid();
};

#endif /* Boid_hpp */
