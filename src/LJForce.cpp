#ifndef _LJFORCE_CPP_
#define _LJFORCE_CPP_

#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
// #include <cmath>
#include <math.h>

#include "mdsys.h"
#include "MVector.h"
#include "LJForce.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////

// LJForce::LJForce(MDSystem* &umdsys) {
//    mds=umdsys;
// }

LJForce::LJForce(std::shared_ptr<MDSystem> umdsys) {
    mds=umdsys;
}

////////////////////////////////////////////////////////////////////////////

void LJForce::force_gh(bool first_int) {
// void LJForce::force_gh() {

    N_colloids=mds->N_colloids;
    box_length=mds->box_length;

    //////////////////////////////////////////////////

    vector<Boid*> boid = mds->boid;
    vector<Boid*> gh_boid = mds->gh_boid;

    vector<MVector*> force = mds->force;
    vector<MVector*> prev_force = mds->prev_force;

    double* torque = mds->torque;
    double* prev_torque = mds->prev_torque;

    bool* gh_flag  = mds->gh_flag;
    // bool first_int = mds->first_int;

    //////////////////////////////////////////////////
    r_seg     = mds->r_seg;
    att_en    = mds->att_en;
    rep_corr  = mds->rep_corr;
    att_corr  = mds->att_corr;
    rep_range = mds->r_cutoff_rep;
    att_range = mds->r_cutoff_att;

    part_thrd = mds->thrd;
    bound_part_thrd = mds->box_length - mds->thrd;

    // double bound_part_thrd_sq = bound_part_thrd*bound_part_thrd;
    double part_thrd_sq = part_thrd*part_thrd;
    double f_prop = mds->f_prop;
    //////////////////////////////////////////////////

    vector<MVector> n_force;
    MVector n_pos_j;

    MVector delta_rij;
    MVector p_delta_rij;
    double rij_2;

    //////////////////////////////////////////////////
    // LOOP OVER ALL COLOIDS
    for (int i = 0; i < N_colloids; i++) {

        //////////////////////////////////////////////////
        // SET FORCES AND TORQUES TO ZERO

        if(first_int == true){
            // printf("first int\n");
            prev_force[i]->set(force[i]->x, force[i]->y);
            prev_torque[i] = torque[i];
        }

        // printf("second int\n");

        force[i]->set(0.0, 0.0);
        torque[i] = 0.0;

        force[i]->x += f_prop * cos(boid[i]->ang);
        force[i]->y += f_prop * sin(boid[i]->ang);

        //////////////////////////////////////////////////

        for (int j = i+1; j < N_colloids; j++) {
            // REAL PARTICLES INTERACTION
            rij_2 = boid[i]->pos.squared_distance(boid[j]->pos);
            if(rij_2 <= part_thrd_sq){
                // printf("parts:%d,%d\n", i,j);
                n_force = boid[i]->force_torque_lennard_jones_types(boid[j], att_range, att_en,
                att_corr, rep_range, rep_corr, r_seg);

                force[i]->x += n_force[0].x;
                force[i]->y += n_force[0].y;

                force[j]->x -= n_force[0].x;
                force[j]->y -= n_force[0].y;

                torque[i] += n_force[1].x;
                torque[j] += n_force[1].y;
            }

            //////////////////////////////////////////////////
            // PARTICLES AND GHOST PARTICLES INTERACTIONS
            if(gh_flag[j] == true){
                rij_2 = boid[i]->pos.squared_distance(gh_boid[j]->pos);
                if(rij_2 <= part_thrd_sq){
                    n_force = boid[i]->force_torque_lennard_jones_types(gh_boid[j], att_range, att_en,
                    att_corr, rep_range, rep_corr, r_seg);

                    force[i]->x += n_force[0].x;
                    force[i]->y += n_force[0].y;

                    force[j]->x -= n_force[0].x;
                    force[j]->y -= n_force[0].y;

                    torque[i] += n_force[1].x;
                    torque[j] += n_force[1].y;
                }
            }

            //////////////////////////////////////////////////

            if(gh_flag[j+N_colloids] == true){
                rij_2 = boid[i]->pos.squared_distance(gh_boid[j+N_colloids]->pos);
                if(rij_2 <= part_thrd_sq){
                    n_force = boid[i]->force_torque_lennard_jones_types(gh_boid[j+N_colloids],
                        att_range, att_en, att_corr, rep_range, rep_corr, r_seg);

                    force[i]->x += n_force[0].x;
                    force[i]->y += n_force[0].y;

                    force[j]->x -= n_force[0].x;
                    force[j]->y -= n_force[0].y;

                    torque[i] += n_force[1].x;
                    torque[j] += n_force[1].y;
                }
            }

            //////////////////////////////////////////////////

            if(gh_flag[j+2*N_colloids] == true){
                rij_2 = boid[i]->pos.squared_distance(gh_boid[j+2*N_colloids]->pos);
                if(rij_2 <= part_thrd_sq){
                    n_force = boid[i]->force_torque_lennard_jones_types(gh_boid[j+2*N_colloids],
                        att_range, att_en, att_corr, rep_range, rep_corr, r_seg);

                    force[i]->x += n_force[0].x;
                    force[i]->y += n_force[0].y;

                    force[j]->x -= n_force[0].x;
                    force[j]->y -= n_force[0].y;

                    torque[i] += n_force[1].x;
                    torque[j] += n_force[1].y;
                }
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////

void LJForce::force_wall_gh(bool first_int) {

    N_colloids   = mds->N_colloids;
    box_length   = mds->box_length;
    box_length_x = mds->box_length_x;
    box_length_y = mds->box_length_y;

    //////////////////////////////////////////////////

    bool* gh_flag         = mds->gh_flag;
    vector<Boid*> boid    = mds->boid;
    vector<Boid*> gh_boid = mds->gh_boid;

    vector<MVector*> force      = mds->force;
    vector<MVector*> prev_force = mds->prev_force;

    double* torque      = mds->torque;
    double* prev_torque = mds->prev_torque;

    // bool first_int = mds->first_int;

    //////////////////////////////////////////////////
    r_seg     = mds->r_seg;
    att_en    = mds->att_en;
    rep_corr  = mds->rep_corr;
    att_corr  = mds->att_corr;
    rep_range = mds->r_cutoff_rep;
    att_range = mds->r_cutoff_att;

    part_thrd = mds->thrd;
    double part_thrd_sq = part_thrd*part_thrd;

    // bound_part_thrd = mds->box_length - mds->thrd;
    // double bound_part_thrd_sq = bound_part_thrd*bound_part_thrd;

    double f_prop = mds->f_prop;
    //////////////////////////////////////////////////

    vector<MVector> n_force;
    vector<MVector> w_force;
    MVector n_pos_j;

    MVector delta_rij;
    MVector p_delta_rij;
    double rij_2;

    //////////////////////////////////////////////////
    // LOOP OVER ALL COLOIDS
    for (int i = 0; i < N_colloids; i++) {

        //////////////////////////////////////////////////
        // SET FORCES AND TORQUES TO ZERO

        if(first_int == true){
            // printf("first int\n");
            prev_force[i]->set(force[i]->x, force[i]->y);
            prev_torque[i] = torque[i];
        }

        //////////////////////////////////////////////////

        torque[i] = 0.0;
        force[i]->set(0.0, 0.0);

        force[i]->x += f_prop * cos(boid[i]->ang);
        force[i]->y += f_prop * sin(boid[i]->ang);

        //////////////////////////////////////////////////

        w_force = boid[i]->force_torque_wall(1.0, 0.0, 0.0, rep_range, rep_corr, r_seg); // left wall
        force[i]->x += w_force[0].x;
        force[i]->y += w_force[0].y;
        torque[i]   += w_force[1].x;

        w_force = boid[i]->force_torque_wall(1.0, 0.0, -box_length_x, rep_range, rep_corr, r_seg); // right wall
        force[i]->x += w_force[0].x;
        force[i]->y += w_force[0].y;
        torque[i]   += w_force[1].x;

        //////////////////////////////////////////////////

        for (int j = i+1; j < N_colloids; j++) {
            // REAL PARTICLES INTERACTION
            rij_2 = boid[i]->pos.squared_distance(boid[j]->pos);
            if(rij_2 <= part_thrd_sq){
                // printf("parts:%d,%d\n", i,j);

                n_force = boid[i]->force_torque_lennard_jones_types(boid[j],
                    att_range, att_en, att_corr, rep_range, rep_corr, r_seg);

                force[i]->x += n_force[0].x;
                force[i]->y += n_force[0].y;

                force[j]->x -= n_force[0].x;
                force[j]->y -= n_force[0].y;

                torque[i] += n_force[1].x;
                torque[j] += n_force[1].y;
            }

            //////////////////////////////////////////////////
            // PARTICLES AND GHOST PARTICLES INTERACTIONS
            if(gh_flag[j] == true){
                rij_2 = boid[i]->pos.squared_distance(gh_boid[j]->pos);
                if(rij_2 <= part_thrd_sq){
                    n_force = boid[i]->force_torque_lennard_jones_types(gh_boid[j], att_range, att_en, att_corr, rep_range, rep_corr, r_seg);

                    force[i]->x += n_force[0].x;
                    force[i]->y += n_force[0].y;

                    force[j]->x -= n_force[0].x;
                    force[j]->y -= n_force[0].y;

                    torque[i] += n_force[1].x;
                    torque[j] += n_force[1].y;
                }
            }
            //////////////////////////////////////////////////
        }
    }
}

//////////////////////////////////////////////////

#endif  /* _LJFORCE_CPP_ */
