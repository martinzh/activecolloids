//
//  Utils.h
//
//  Created by Martin Zumaya on 1/16/18.
//

#ifndef Utils_h
#define Utils_h

#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <math.h>

#include "MVector.h"
#include "Boid.h"

class Utils {

    public:

    static double randMtoN(double M, double N);

    static bool lineIntersect(MVector start1, MVector end1, MVector start2, MVector end2);
    static bool overlap(Boid* boid_a, Boid* boid_b);
    static bool min_dist(Boid* boid_a, Boid* boid_b, double d);
    static bool general_overlap(vector<Boid*> boid, double d);

    static void arrange_boids(vector<Boid*> boid, double Lx, double Ly,
        double tol, double r, double scale, double d, int species, double mass);

    Utils();

};

#endif
