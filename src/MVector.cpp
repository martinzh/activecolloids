//
//  MVector.cpp
//
//  Created by Martin Zumaya on 1/16/18.
//

#include "MVector.h"
#include <math.h>

//////////// CONSTRUCTORS ////////////

MVector::MVector(){
    x = 0.0;
    y = 0.0;
    size = 2;
}

MVector::MVector(double xx, double yy){
    x = xx;
    y = yy;
    size = 2;
}

MVector::MVector(const MVector &vec){
    x = vec.x;
    y = vec.y;
    size = 2;
}

//////////// LINEAR INTERPOLATION 2 VECTORS ////////////

MVector MVector::getInterpolated(MVector dir, double p){

    MVector interp_vec;

    interp_vec.x = (1.0-p)*x + p*dir.x;
    interp_vec.y = (1.0-p)*y + p*dir.y;

    return interp_vec;
}

//////////// ROTATE VECTOR (MODIFIES IT) ////////////

void MVector::rotateRad(double angle, MVector pivot){

    double x_rotated;
    double y_rotated;

    x_rotated = ((x - pivot.x) * cos(angle)) - ((y - pivot.y) * sin(angle)) + pivot.x;
    y_rotated = ((x - pivot.x) * sin(angle)) + ((y - pivot.y) * cos(angle)) + pivot.y;

    x = x_rotated;
    y = y_rotated;
}

//////////// GET ROTATED VECTOR ////////////

MVector MVector::getRotatedRad(double angle, MVector pivot){

    MVector rotated;

    rotated.x = ((x - pivot.x) * cos(angle)) - ((y - pivot.y) * sin(angle)) + pivot.x;
    rotated.y = ((x - pivot.x) * sin(angle)) + ((y - pivot.y) * cos(angle)) + pivot.y;

    return rotated;
}

//////////// VECTOR LENGTH ////////////

double MVector::length(){
    return sqrt(x*x + y*y);
}

double MVector::squared_length(){
    return x*x + y*y;
}

//////////// NORMALIZE VECTOR ////////////

void MVector::normalize(){
    double norm = sqrt(x*x + y*y);
    if(norm > 0.0){
        x /= norm;
        y /= norm;
    }
}

//////////// UPDATE VECTOR COORDINATES ////////////

void MVector::set(MVector vec){
    x = vec.x;
    y = vec.y;
}

void MVector::set(double xx, double yy){
    x = xx;
    y = yy;
}

//////////// SCALE VECTOR ////////////

void MVector::scale(double sc){
    x *= sc;
    y *= sc;
}

//////////// COMPUTE DISTANCE BETWEEN 2 POINTS ////////////

double MVector::squared_distance(MVector vec){
    return (x - vec.x)*(x - vec.x) + (y - vec.y)*(y - vec.y);
}

double MVector::distance(MVector vec){
    return sqrt((x - vec.x)*(x - vec.x) + (y - vec.y)*(y - vec.y));
}
