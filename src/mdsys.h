#ifndef _MDSYS_H_
#define _MDSYS_H_

#include <string>
using std::string;

#include "MVector.h"
#include "Boid.h"

class MDSystem {
public:

   MDSystem(int spc, int _nboids, double _box_length, double ratio, double _r, double _visc, double _dt, double _f_prop, double _att_en, double _temp);

   ~MDSystem();

   /////////////////////////////////////

   int N_colloids, spc;
   double box_length, box_length_x, box_length_y, att_en, r, r_seg, tol, boid_scale;
   double thrd, r_cutoff_att, r_cutoff_rep, r_cutoff_att_s, r_cutoff_rep_s;
   double att_corr, rep_corr;
   double visc, t_visc, n_visc, ang_n_visc;
   double dt, temp, mass, f_prop, b_verl;

   /////////////////////////////////////

   vector<Boid*> boid;
   vector<Boid*> gh_boid;

   vector<MVector*> force;
   vector<MVector*> prev_force;

   double *torque;
   double *prev_torque;

   bool *gh_flag;
   // bool first_int;

   /////////////////////////////////////

protected:
};


#endif
