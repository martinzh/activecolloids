/////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/////////////////////////////////////

#include <fstream>
#include <sstream>
#include <iostream>

/////////////////////////////////////

#include <sys/stat.h>

/////////////////////////////////////

#include <time.h>
#include <vector>
#include <math.h>
#include <random>

#include "MVector.h"
#include "Boid.h"
#include "Utils.h"
#include "mdsys.h"
#include "LJForce.h"
#include "overdamped.h"

using namespace std;

int main(int argc, char const *argv[]) {

    /////////////////////////////////////

    int spc           = stoi(argv[1]); // MOLECULE SPECIE
    int N             = stoi(argv[2]); // NUMBER OF PARTICLES
    double box_length = stod(argv[3]); // SIZE OF BOX
    double ratio      = stod(argv[4]); // ASPECT RATIO
    double f_sp       = stod(argv[5]); // SELF-PROPULSION FORCE
    double visc       = stod(argv[6]); // VISCOSITY
    double temp       = stod(argv[7]); // TEMPERATURE
    double dt         = stod(argv[8]); // INTEGRATION TIMESTEP
    int T             = stoi(argv[9]); // NUMBER OF ITERATIONS
    int rep           = stoi(argv[10]); // NUMBER OF REPETITION

    /////////////////////////////////////

    // int spc           = stoi(argv[1]);
    // double f_sp       = stod(argv[2]);
    // double ratio      = stod(argv[3]);
    // double temp       = stod(argv[4]);
    // int T             = stoi(argv[5]);
    // int rep           = stoi(argv[6]);
    //
    // int N             = 512;
    // double box_length = 135.0;
    // double dt         = 0.001;
    // double visc       = 250.0;

    /////////////////////////////////////

    bool first_int = true;

    /////////////////////////////////////

    srand(time(NULL)+rep);

    // MDSystem(int _spc, int _N_colloids, double _box_length, double ratio, double _r, double _visc, double _dt, double _f_prop, double _att_en, double _temp)

    //std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, 11,   15.0, 1.0, 0.25, visc, dt, f_sp, 1.0, temp));
    //std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, 256,  90.0, 1.0, 0.25, visc, dt, f_sp, 1.0, temp));
    //std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, 128,  45.0, 1.0, 0.25, visc, dt, f_sp, 1.0, temp));
    //std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, 512, 135.0, 1.0, 0.25, visc, dt, f_sp, 1.0, temp));
    //std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, 1024,200.0, 1.0, 0.25, visc, dt, f_sp, 1.0, temp));
    //std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, 1024,200.0, 1.0, 0.25, visc, dt, f_sp, 1.0, temp));
    std::shared_ptr<MDSystem> mdsys(new MDSystem(spc, N, box_length, ratio, 0.25, visc, dt, f_sp, 1.0, temp));

    std::shared_ptr<LJForce> ljforce(new LJForce(mdsys));
    std::shared_ptr<Overdamped> overdamped(new Overdamped(mdsys, rep));

    /////////////////////////////////////

    stringstream ss_top;
    stringstream ss_out;
    std::string s_tmp;

    /////////////////////////////////////
    // CREATE DATA FOLDER

    // ss_out << "/home/zumaya/SA_CH_Data/data_SP_" << spc;
    ss_top << "/storage/zumaya_g/zumaya/SA_CH_Data/data_SP_" << spc;

    s_tmp = ss_top.str();
    const int dir_err = mkdir(s_tmp.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    if (-1 == dir_err){
        printf("Folder already exists\n");
    }

    ss_out.str("");

    /////////////////////////////////////
    // CREATE PARAMETER DATA FOLDER

    // ss_out << "/home/zumaya/SA_CH_Data/data_SP_" << spc << "/data_N_" << mdsys->N_colloids
    //     << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio;

    ss_out << ss_top.str()
        << "/data_N_" << mdsys->N_colloids
        << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio;

    s_tmp = ss_out.str();
    const int dir_err_1 = mkdir(s_tmp.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    if (-1 == dir_err_1){
        printf("Folder already exists\n");
    }

    ss_out.str("");

    /////////////////////////////////////
    // ARCHIVO DE POSICIONES
    // ss_out << "/home/zumaya/SA_CH_Data/data_SP_" << spc << "/data_N_" << mdsys->N_colloids
    //     << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio << "/pos_" << rep << ".dat";

    ss_out << ss_top.str()
        << "/data_N_" << mdsys->N_colloids
        << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio << "/pos_" << rep << ".dat";
    s_tmp = ss_out.str();

    std::ofstream pos_out = std::ofstream(s_tmp.c_str(), std::ios::binary);
    ss_out.str("");

    /////////////////////////////////////
    // ARCHIVO DE VELOCIDADES

    // ss_out << "/home/zumaya/SA_CH_Data/data_SP_" << spc << "/data_N_" << mdsys->N_colloids
    //     << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio << "/pos_" << rep << ".dat";

    ss_out << ss_top.str()
        << "/data_N_" << mdsys->N_colloids
        << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio << "/vel_" << rep << ".dat";

    s_tmp = ss_out.str();

    std::ofstream vel_out = std::ofstream(s_tmp.c_str(), std::ios::binary);
    ss_out.str("");

    /////////////////////////////////////
    // ARCHIVO DE ORIENTACIONES
    // ss_out << "/home/zumaya/SA_CH_Data/data_SP_" << spc << "/data_N_" << mdsys->N_colloids
    //     << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio << "/ang_" << rep << ".dat";

    ss_out << ss_top.str()
        << "/data_N_" << mdsys->N_colloids
        << "_FSP_" << f_sp << "_T_" << temp << "_R_" << ratio << "/ang_" << rep << ".dat";
    s_tmp = ss_out.str();

    std::ofstream ang_out = std::ofstream(s_tmp.c_str(), std::ios::binary);

    /////////////////////////////////////

    Utils::arrange_boids(mdsys->boid, mdsys->box_length_x, mdsys->box_length_y,
        mdsys->boid_scale, mdsys->r, mdsys->boid_scale, mdsys->tol, spc, mdsys->mass);

    overdamped->set_ghosts_channel();

    printf("Finished arranging colloids\n");

    //////////////////////////////////////////////////////////////////////////

    // int segs_num = gh_boid->segs.size();
    // int tot_segs_num = 2*mdsys->N_colloids*segs_num;
    // double all_segs_pos[tot_segs_num];

    int verts_num = mdsys->boid[0]->verts.size();
    int tot_verts_num = 2*mdsys->N_colloids*verts_num;
    double all_verts_pos[tot_verts_num];

    double all_coll_vel[2 * mdsys->N_colloids];
    double all_angs[mdsys->N_colloids];

    int g_id;
    int t_st, t_end;

    //////////////////////////////////////////////////////////////////////////
    // OUTPUT INITIAL CONDITIONS

    for(int id=0; id < mdsys->N_colloids; id++) {

        all_angs[id] = mdsys->boid[id]->ang;

        all_coll_vel[2 * id] = mdsys->boid[id]->vel.x;
        all_coll_vel[2 * id + 1] = mdsys->boid[id]->vel.y;

        for( int j = 0; j < verts_num; j ++){
            g_id = 2*(id*verts_num + j);
            all_verts_pos[g_id]   = mdsys->boid[id]->verts[j].x;
            all_verts_pos[g_id+1] = mdsys->boid[id]->verts[j].y;
        }

        // for( int j = 0; j < segs_num; j ++){
        //     g_id = 2*(id*segs_num + j);
        //     all_segs_pos[g_id]   = boid[id]->segs[j].x;
        //     all_segs_pos[g_id+1] = boid[id]->segs[j].y;
        // }
    }

    // WRITE SEGMETS POSITIONS TO FILE
    // printf("//// 0\n");

    // pos_out.write(reinterpret_cast<const char*>(&all_segs_pos), tot_segs_num*sizeof(double));
    pos_out.write(reinterpret_cast<const char*>(&all_verts_pos), tot_verts_num*sizeof(double));
    vel_out.write(reinterpret_cast<const char*>(&all_coll_vel),  2*mdsys->N_colloids * sizeof(double));
    ang_out.write(reinterpret_cast<const char*>(&all_angs),      mdsys->N_colloids*sizeof(double));

    //////////////////////////////////////////////////////////////////////////

    for( int tt = 0; tt < T; tt ++){

        t_st  = int(pow(10, tt));
        t_end = int(pow(10, tt+1));

        ///////////////////////////////////////////////////////

        for(int t = t_st; t < t_end; t ++){

            // printf("//// %d\n", t);

            ///////////////////////////////////////////////////////
            // WRITE COLLOID'S SEGMENTS POSITIONS

            if( t % t_st == 0 || t % (t_st/10) == 0){

                printf("//// %d\n", t);

                for(int id=0; id < mdsys->N_colloids; id++) {

                    all_angs[id] = mdsys->boid[id]->ang;

                    all_coll_vel[2 * id] = mdsys->boid[id]->vel.x;
                    all_coll_vel[2 * id + 1] = mdsys->boid[id]->vel.y;

                    for( int j = 0; j < verts_num; j ++){
                    g_id = 2*(id*verts_num + j);
                    all_verts_pos[g_id]   = mdsys->boid[id]->verts[j].x;
                    all_verts_pos[g_id+1] = mdsys->boid[id]->verts[j].y;
                    }

                    // for( int j = 0; j < segs_num; j ++){
                    //     g_id = 2*(id*segs_num + j);
                    //     all_segs_pos[g_id]   = boid[id]->segs[j].x;
                    //     all_segs_pos[g_id+1] = boid[id]->segs[j].y;
                    // }
                }

                // WRITE SEGMETS POSITIONS TO FILE
                // pos_out.write(reinterpret_cast<const char*>(&all_segs_pos), tot_segs_num*sizeof(double));
                pos_out.write(reinterpret_cast<const char*>(&all_verts_pos), tot_verts_num*sizeof(double));
                vel_out.write(reinterpret_cast<const char*>(&all_coll_vel),  2*mdsys->N_colloids * sizeof(double));
                ang_out.write(reinterpret_cast<const char*>(&all_angs),      mdsys->N_colloids*sizeof(double));

            }

            //////////////////////////////////////////////////////////////////////////

            first_int = true;
            overdamped->set_ghosts_channel();
            ljforce->force_wall_gh(first_int);

            overdamped->update_verlet_pos();

            first_int = false;
            overdamped->set_ghosts_channel();
            ljforce->force_wall_gh(first_int);

            overdamped->update_verlet_vel();
            overdamped->check_bound_channel();

            for(int i = 0; i < mdsys->N_colloids; i++){
                // if((mdsys->force[i]->x>0.0 || mdsys->force[i]->y>0.0) && mdsys->torque[i]>0.0) printf("%d:%f,%f,%f||\n", i, mdsys->force[i]->x, mdsys->force[i]->y, mdsys->torque[i]);
                if(fabs(mdsys->boid[i]->pos.x) > mdsys->box_length_x + 2.0 ){
                    printf("/////t::%d\tBLOW !! %d|X:%f,Y:%f|FX:%f,FY:%f|T:%f\n",
                        t, i, mdsys->boid[i]->pos.x, mdsys->boid[i]->pos.y, mdsys->force[i]->x,
                        mdsys->force[i]->y, mdsys->torque[i]);
                    exit(-1);
                }
            }
            // printf("\n");
        }
    }

    //////////////////////////////////////////////////////////////////////////

    pos_out.close();
    vel_out.close();
    ang_out.close();

    printf("Done!\n");

    return 0;
}
