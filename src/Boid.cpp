//
//  Boid.cpp
//
//  Created by Martin Zumaya on 1/16/18.
//

////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include "Utils.h"
#include "MVector.h"
#include "Boid.h"

////////////////////////////////////////////////////////////////////////////

Boid::Boid(){
};

////////////////////////////////////////////////////////////////////////////
Boid::~Boid() {
    //
    // for(int i=0; i<verts.size(); i++) {
    //         delete verts[i];
    // }
    //
    // for(int i=0; i<segs.size(); i++) {
    //         delete verts[i];
    // }
}

////////////////////////////////////////////////////////////////////////////

void Boid::setup(int spc, double box_length_x, double box_length_y, double tol, double r, double scale, double mass){

    // DEFINE VERTICES DE POLIGONO

    switch (spc) {
    // TRIANGULO
    case 1:{
        verts.push_back(scale*MVector(1.0, 0.0));
        verts.push_back(scale*MVector(0.0, 0.5));
        verts.push_back(scale*MVector(0.0, -0.5));
    } break;

    // TRIANGULO
    case 6:{
        verts.push_back(scale*MVector(1.0, 0.0));
        verts.push_back(scale*MVector(0.0, 0.5));
        verts.push_back(scale*MVector(0.0, -0.5));
    } break;

    // RECTANGULO
    case 2:{
        verts.push_back(scale*MVector(0.5, 0.5));
        verts.push_back(scale*MVector(0.5, 0.0));
        verts.push_back(scale*MVector(-0.5, 0.0));
        verts.push_back(scale*MVector(-0.5, 0.5));
    } break;

    // CUADRADO
    case 3:{
        verts.push_back(scale*MVector(0.5, 0.5));
        verts.push_back(scale*MVector(0.5, -0.5));
        verts.push_back(scale*MVector(-0.5, -0.5));
        verts.push_back(scale*MVector(-0.5, 0.5));
    } break;

    // CUADRADO
    case 4:{
        verts.push_back(scale*MVector(0.5, 0.5));
        verts.push_back(scale*MVector(0.5, -0.5));
        verts.push_back(scale*MVector(-0.5, -0.5));
        verts.push_back(scale*MVector(-0.5, 0.5));
    } break;

    // PEZ
    case 5:{
        verts.push_back(scale*MVector(0.5, 0.5));
        verts.push_back(scale*MVector(0.75, 0.0));
        verts.push_back(scale*MVector(0.5, -0.5));
        verts.push_back(scale*MVector(-0.5, -0.5));
        verts.push_back(scale*MVector(-0.25, -0.0));
        verts.push_back(scale*MVector(-0.5, 0.5));
    } break;

    default:{printf("Entered default\n");}break;
    }

    //////////////////////////////////////

    // CALCULA CENTRO DE MASA
    MVector c_mass;

    for (size_t i = 0; i<verts.size(); i++) {
        c_mass += verts[i];
    }
    c_mass /= double(verts.size());

    //////////////////////////////////////

    // CALCULA COORDENADAS DE SEGMENTOS

    size_t n_v = verts.size();
    double d;
    double t = 0.0;

    // CLOSED POLYGONS
    for(size_t i = 0; i < n_v; i ++){
        d = verts[i%n_v].distance(verts[(i+1)%n_v]);
        t = 0.0;

        if(d > 1.5){
            do{
                segs.push_back(verts[i%n_v].getInterpolated(verts[(i+1)%n_v], t ));
                t += 0.5*r;
            } while(t < 1.0);
        }else{
            do{
                segs.push_back(verts[i%n_v].getInterpolated(verts[(i+1)%n_v], t ));
                t += r;
            } while(t < 1.0);
        }
    }

    // OPEN SHAPES
    // for(int i = 0; i < n_v-1; i ++){
    //     d = verts[i].distance(verts[i+1]);
    //     t = 0.0;
    //
    //     if(d > 1.5){
    //         do{
    //             segs.push_back(verts[i].getInterpolated(verts[i+1], t ));
    //             t += 0.5*r;
    //         } while(t < 1.0);
    //     }else{
    //         do{
    //             segs.push_back(verts[i].getInterpolated(verts[i+1], t ));
    //             t += r;
    //         } while(t < 1.0);
    //     }
    // }

    type.assign(segs.size(), 1);

    switch (spc) {
    // TRIANGULO
    case 1:{
        // type[0] = -1;
        // set_side(0,1,-1,r);
        // set_side(2,3,-1,r);
        set_side(0,1,2,r);
        set_side(2,3,3,r);
    } break;

    // TRIANGULO
    case 6:{
        double prob = Utils::randMtoN(0.0,1.0);
        if( prob < 0.5){
            set_side(0,1,2,r);
            set_side(2,3,3,r);
        }else{
            set_side(0,1,3,r);
            set_side(2,3,2,r);
        }
    }break;

    // RECTANGULO (aristas)
    // propulsion en la direccion de las aristas atractivas
    case 2:{
        set_side(0, 1, -1, r);
        set_side(2, 3, -1, r);
    } break;

    // CUADRADO (aristas)
    // propulsion en la direccion de las aristas atractivas
    case 3:{
        // set_side(0, 1, -1, r);
        // set_side(2, 3, -1, r);
        set_side(0, 1, 2, r);
        set_side(2, 3, 3, r);
    } break;

    // propulsion en la direccion de las aristas repulsivas
    case 4:{
        // set_side(1, 2, -1, r);
        // set_side(3, 4, -1, r);
        set_side(1, 2, 2, r);
        set_side(3, 4, 3, r);
    } break;

    // PEZ
    case 5:{
        // set_side(0, 2, -1, r);
        // set_side(3, 5, -1, r);
        set_side(0, 2, 2, r);
        set_side(3, 5, 3, r);
    } break;

    default:{printf("Entered default\n");}break;
    }

    // CUADRADO (esquina)
    // set_side(0, 2, -1, r);

    //////////////////////////////////////

    // MASA TOTAL Y MOMENTO DE INERCIA DEL BOID
    // tot_mass  = mass * (double)segs.size();
    b_mass    = 1.0;
    inertia_m = compute_inertia_moment(b_mass);

    //////////////////////////////////////

    // TRASLADA VERTICES A CENTRO DE MASA
    for (size_t i = 0; i<verts.size(); i++) {
        verts[i] -= c_mass;
    }

    for (size_t i = 0; i<segs.size(); i++) {
        segs[i] -= c_mass;
    }

    //////////////////////////////////////

    // ASIGNA POSICION Y ORIENTACION ALEATORIA
    pos.set(Utils::randMtoN(tol, box_length_x - tol), Utils::randMtoN(tol, box_length_y - tol));
    vel.set(0.0, 0.0);

    ang = Utils::randMtoN(0, 2.0*M_PI);

    //////////////////////////////////////

    // ROTA Y TRASLADA VERTICES Y SEGMENTOS CON RESPECTO AL CENTRO DE MASA
    for (size_t i =0; i<verts.size(); i++) {
        verts[i] += pos;
        verts[i].rotateRad(ang - atan2(0.0, 1.0), pos);
    }

    for (size_t i =0; i<segs.size(); i++) {
        segs[i] += pos;
        segs[i].rotateRad(ang - atan2(0.0, 1.0), pos);
    }

    //////////////////////////////////////
    prev_pos.set(0.0, 0.0);

    w_ang    = 0.0;
    prev_ang = 0.0;

    x_n = 0.0;
    y_n = 0.0;
    x_n_a = 0.0;
    y_n_a = 0.0;

    //////////////////////////////////////
}

////////////////////////////////////////////////////////////////////////////

double Boid::compute_inertia_moment(double m){

    double seg_num = 0.0;
    double seg_den = 0.0;
    double inertia_m = 0.0;

    for (size_t i = 0; i < verts.size()-1; i++) {
        seg_num += (verts[i].x * verts[i].x + verts[i].y * verts[i].y + verts[i].x * verts[i+1].x
            + verts[i].y * verts[i+1].y + verts[i+1].x * verts[i+1].x + verts[i+1].y * verts[i+1].y)
            * (verts[i].x * verts[i+1].y - verts[i+1].x * verts[i].y);
        seg_den += verts[i].x * verts[i+1].y - verts[i+1].x * verts[i].y;
    }

    inertia_m = (m/6.0) * (seg_num / seg_den);

    return inertia_m;
}

////////////////////////////////////////////////////////////////////////////

// ASIGNA ETIQUETA DE TIPO A ARISTA
void Boid::set_side(int i_vert, int f_vert, int id, double r){

    double d,t;
    int id_start, id_end, i, n_v;

    n_v = verts.size();

    i = 0;
    id_start = 1;
    id_end = 0;

    while(i < f_vert){
        d = verts[i].distance(verts[(i+1)%n_v]);
        t = 0.0;

        if(d > 1.5){
            do{
                if(i < i_vert) id_start ++;
                if(i <= f_vert) id_end ++;
                t += 0.5*r;
            } while(t < 1.0);
        }else{
            do{
                if(i < i_vert) id_start ++;
                if(i <= f_vert) id_end ++;
                t += r;
            } while(t < 1.0);
        }
        i ++;
    }

    for(int j = id_start; j < id_end; j ++){
        type[j] = id;
    }

}

////////////////////////////////////////////////////////////////////////////
// ASIGNA VALORES DE OTRO Boid
void Boid::set(Boid* boid){
    pos.set(boid->pos);
    vel.set(boid->vel);

    ang = boid->ang;
    w_ang = (boid->w_ang);

    for(size_t i = 0; i < boid->segs.size(); i ++){
        segs[i].set(boid->segs[i]);
    }

    for(size_t i = 0; i < boid->verts.size(); i ++){
        verts[i].set(boid->verts[i]);
    }
}

////////////////////////////////////////////////////////////////////////////

void Boid::translate_boid(MVector dir){

    for(size_t j = 0; j < segs.size(); j++){
        segs[j].x += dir.x - pos.x;
        segs[j].y += dir.y - pos.y;
    }
    for(size_t j = 0; j < verts.size(); j++){
        verts[j].x += dir.x - pos.x;
        verts[j].y += dir.y - pos.y;
    }

    pos.set(dir);

}

////////////////////////////////////////////////////////////////////////////

double Boid::line_point_distance(double a, double b, double c, double x0, double y0){
    return fabs(a*x0 + b*y0 + c) / sqrt(a*a + b*b);
}

////////////////////////////////////////////////////////////////////////////

MVector Boid::line_point_vector(double a, double b, double c, double x0, double y0){
    MVector point;

    point.x = (b*(b*x0 - a*y0) - a*c) / (a*a + b*b);
    point.y = (a*(-b*x0 + a*y0) - b*c) / (a*a + b*b);

    return point;
}

////////////////////////////////////////////////////////////////////////////

double Boid::lennard_jones_force_coef(double epsilon, double r_inv_2, double r_seg_2, double r_2, double r_corr){

    double r_inv_6 = pow(r_seg_2*r_inv_2, 3.0);
    return 48.0 * epsilon * r_inv_2 * (r_inv_6 * (r_inv_6 - 0.5) - r_2 * r_corr);

}

////////////////////////////////////////////////////////////////////////////

vector<MVector> Boid::force_torque_lennard_jones(Boid* boid_j, double att_range, double att_en,
    double att_corr, double rep_range, double rep_corr, double r_seg){

    MVector f_dir; // force direction
    MVector f_exc; // point in wall

    vector<MVector> force;
    force.push_back(MVector());
    force.push_back(MVector());

    force[0].set(0.0, 0.0);
    force[1].set(0.0, 0.0);

    double rab_2;
    double inv_rab_2;
    double att_range_sq = att_range * att_range;
    double rep_range_sq = rep_range * rep_range;
    double r_seg_sq     = r_seg * r_seg;
    double force_coeff;

    for (size_t a = 0; a < segs.size(); a++) {
        for (size_t b = 0; b < segs.size(); b++) {

            rab_2 = segs[a].squared_distance(boid_j->segs[b]);
            inv_rab_2 = 1.0 / rab_2;

            if(type[a] == -1 && boid_j->type[b] == -1){
                if(rab_2 < att_range_sq){
                    f_dir = segs[a] - boid_j->segs[b];

                    force_coeff = lennard_jones_force_coef(att_en, inv_rab_2, r_seg_sq, rab_2, att_corr);

                    force[0].x += force_coeff * f_dir.x;
                    force[0].y += force_coeff * f_dir.y;

                    force[1].x += force_coeff *
                        (f_dir.y * (segs[a].x - pos.x) - f_dir.x * (segs[a].y - pos.y));
                    force[1].y += force_coeff *
                        (-f_dir.y * (boid_j->segs[b].x - boid_j->pos.x) + f_dir.x * (boid_j->segs[b].y - boid_j->pos.y));
                }
            }else{
                if(rab_2 < rep_range_sq){
                    f_dir = segs[a] - boid_j->segs[b];

                    force_coeff = lennard_jones_force_coef(att_en, inv_rab_2, r_seg_sq, rab_2, att_corr);

                    force[0].x += force_coeff * f_dir.x;
                    force[0].y += force_coeff * f_dir.y;

                    force[1].x += force_coeff *
                        (f_dir.y * (segs[a].x - pos.x) - f_dir.x * (segs[a].y - pos.y));
                    force[1].y += force_coeff *
                        (-f_dir.y * (boid_j->segs[b].x - boid_j->pos.x) + f_dir.x * (boid_j->segs[b].y - boid_j->pos.y));
                    }
            }
        }
    }
    // printf("f.x=%f, f.y=%f, t_i=%f, t_j=%f\n", force[0].x, force[0].y, force[1].x, force[1].y);
    return force;
}

////////////////////////////////////////////////////////////////////////////

vector<MVector> Boid::force_torque_lennard_jones_types(Boid* boid_j, double att_range,
    double att_en, double att_corr, double rep_range, double rep_corr, double r_seg){

    MVector f_dir; // force direction
    MVector f_exc; // point in wall

    vector<MVector> force;
    force.push_back(MVector());
    force.push_back(MVector());

    force[0].set(0.0, 0.0);
    force[1].set(0.0, 0.0);

    double rab_2, inv_rab_2, force_coeff;

    double r_seg_sq     = r_seg*r_seg;
    double att_range_sq = att_range * att_range;
    double rep_range_sq = rep_range * rep_range;

    double epsilon      = 0.025;
    double r_cut_sq     = pow(r_seg - epsilon, 2.0);
    double inv_r_cut_sq = 1.0 / r_cut_sq;

    for (size_t a = 0; a < segs.size(); a++) {
        for (size_t b = 0; b < segs.size(); b++) {

            rab_2 = segs[a].squared_distance(boid_j->segs[b]);
            inv_rab_2 = 1.0 / rab_2;

            if(type[a] * boid_j->type[b] == 6){

                if(rab_2 < att_range_sq){
                    // printf("att_segs:%d,%d\n", a,b);
                    f_dir = segs[a] - boid_j->segs[b];

                    if(rab_2 - r_seg_sq <= epsilon){
                        // printf("clipping att force:%d,%d,%f\n", a, b,force_coeff);
                        force_coeff = lennard_jones_force_coef(1.0, inv_r_cut_sq, r_seg_sq, r_cut_sq, rep_corr);
                    }else{
                        force_coeff = lennard_jones_force_coef(att_en, inv_rab_2, r_seg_sq, rab_2, att_corr);
                    }

                    force[0].x += force_coeff * f_dir.x;
                    force[0].y += force_coeff * f_dir.y;

                    force[1].x += force_coeff * (f_dir.y * (segs[a].x - pos.x) - f_dir.x * (segs[a].y - pos.y));
                    force[1].y += force_coeff *
                        (-f_dir.y * (boid_j->segs[b].x - boid_j->pos.x) + f_dir.x * (boid_j->segs[b].y - boid_j->pos.y));
                }
            }else{
                if(rab_2 < rep_range_sq){

                // printf("///// rep_segs:%d,%d\n", a,b);

                f_dir = segs[a] - boid_j->segs[b];

                if(rab_2 - r_seg_sq <= epsilon){
                    // printf("clipping rep force:%d,%d,%f\n", a, b,force_coeff);
                    force_coeff = lennard_jones_force_coef(1.0, inv_r_cut_sq, r_seg_sq, r_cut_sq, rep_corr);
                }else{
                    force_coeff = lennard_jones_force_coef(1.0, inv_rab_2, r_seg_sq, rab_2, rep_corr);
                }

                force[0].x += force_coeff * f_dir.x;
                force[0].y += force_coeff * f_dir.y;

                force[1].x += force_coeff * (f_dir.y * (segs[a].x - pos.x) - f_dir.x * (segs[a].y - pos.y));
                force[1].y += force_coeff *
                    (-f_dir.y * (boid_j->segs[b].x - boid_j->pos.x) + f_dir.x * (boid_j->segs[b].y - boid_j->pos.y));
                }
            }
        }
    }
    return force;
}

////////////////////////////////////////////////////////////////////////////

vector<MVector> Boid::force_torque_wall(double a, double b, double c, double rep_range, double rep_corr, double r_seg){

    MVector f_dir; // force direction
    MVector f_exc; // partial force
    MVector l_point; // point in wall

    vector<MVector> force;
    force.push_back(MVector());
    force.push_back(MVector());

    force[0].set(0.0, 0.0);
    force[1].set(0.0, 0.0);

    double r_sq;
    double inv_r_sq;
    double force_coeff;

    double epsilon      = 0.025;
    double r_seg_sq     = r_seg * r_seg;
    double r_cut_sq     = pow(r_seg - epsilon, 2.0);
    double inv_r_cut_sq = 1.0 / r_cut_sq;
    double rep_range_sq = rep_range * rep_range;

    for (size_t i = 0; i < segs.size(); i++) {
        l_point.set(line_point_vector(a, b, c, segs[i].x, segs[i].y));

        r_sq = l_point.squared_distance(segs[i]);
        inv_r_sq = 1.0 / r_sq;

        if(r_sq < rep_range_sq){

            f_dir.set(segs[i] - l_point);

            if(r_sq - r_seg_sq <= epsilon){
                force_coeff = lennard_jones_force_coef(1.0, inv_r_cut_sq, r_seg_sq, r_cut_sq, rep_corr);
            }else{
                force_coeff = lennard_jones_force_coef(1.0, inv_r_sq, r_seg_sq, r_sq, rep_corr);
            }

            force[0].x += force_coeff * f_dir.x;
            force[0].y += force_coeff * f_dir.y;

            force[1].x += force_coeff * (f_dir.y * (segs[i].x - pos.x) - f_dir.x * (segs[i].y - pos.y));
        }
    }
    return force;
}
////////////////////////////////////////////////////////////////////////////
