#ifndef _LJFORCE_H
#define _LJFORCE_H

#include <memory>

class LJForce {
    public:

    // LJForce(MDSystem* &umdsys);
    LJForce(std::shared_ptr<class MDSystem> umdsys);

    int N_colloids;

    double rep_range_sq;
    double att_range_sq;

    double rep_range;
    double att_range;

    double bound_part_thrd;
    double part_thrd;

    double r_seg;
    double att_en;

    double att_corr;
    double rep_corr;

    double box_length, box_length_x, box_length_y;

    void force_gh(bool first_int);
    void force_wall_gh(bool first_int);
    // void force_gh();

    protected:
    // MDSystem* mds;
    std::shared_ptr<class MDSystem> mds;
};

#endif  /* _LJFORCE_H_ */
