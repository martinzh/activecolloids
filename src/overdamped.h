#ifndef _OVERDAMPED_H_
#define _OVERDAMPED_H_

#include <memory>
#include <random>

#include "mdsys.h"

/* ************************************************************************** */
class Overdamped {
/* ************************************************************************** */
public:
   // Overdamped(MDSystem* &umdsys);
   Overdamped(std::shared_ptr<class MDSystem> umdsys, int rep);

   std::random_device rd;
   std::mt19937 rng;
   std::normal_distribution<double> dist;

   double f_prop,t_visc,n_visc,ang_n_visc;
   // bool first_int;

   void check_bound();
   void check_bound_channel();

   void set_ghosts();
   void set_ghosts_channel();

   void update_verlet_pos();
   void update_verlet_vel();
/* ************************************************************************** */
protected:
   // MDSystem* mds;
   std::shared_ptr<class MDSystem> mds;
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _OVERDAMPED_H_ */
