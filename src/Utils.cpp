//
//  Utils.cpp
//
//  Created by Martin Zumaya on 1/16/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// #include <time.h>

#include <sys/stat.h>
//#include <sys/types.h>

#include "Utils.h"

Utils::Utils(){}

//--------------------------------------------------------------

double Utils::randMtoN(double M, double N){
    return M + (rand() / ( RAND_MAX / (N-M) ) ) ;
}

//--------------------------------------------------------------
bool Utils::lineIntersect(MVector start1, MVector end1, MVector start2, MVector end2){

    double denom = ((end1.x - start1.x) * (end2.y - start2.y))
        - ((end1.y - start1.y) * (end2.x - start2.x));

    if(denom == 0){
        return false;
    }

    double numer = ((start1.y - start2.y) * (end2.x - start2.x))
        - ((start1.x - start2.x) * (end2.y - start2.y));

    double r = numer / denom;

    double numer2 = ((start1.y - start2.y) * (end1.x - start1.x))
        - ((start1.x - start2.x) * (end1.y - start1.y));

    double s = numer2 / denom;

    if ((r < 0 || r > 1) || (s < 0 || s > 1)){return false;}

    return true;
}

//--------------------------------------------------------------
bool Utils::overlap(Boid* boid_a, Boid* boid_b){

    int n_v = boid_a->verts.size();

    for(int i = 0; i < n_v; i ++){
        for(int j = 0; j < n_v; j ++){
            if(lineIntersect(boid_a->verts[i%n_v], boid_a->verts[(i+1)%n_v],
                boid_b->verts[j%n_v], boid_b->verts[(j+1)%n_v]) == true) return true;
        }
    }

    return false;
}

//--------------------------------------------------------------
bool Utils::min_dist(Boid* boid_a, Boid* boid_b, double d){

    int n_v = boid_a->segs.size();

    for(int i = 0; i < n_v; i ++){
        for(int j = 0; j < n_v; j ++){
            if(boid_a->segs[i].distance(boid_b->segs[j]) <= d){
                // printf("seg_%d\tseg_%d\n", i, j);
                return true;
            }
        }
    }

    return false;
}

//--------------------------------------------------------------
bool Utils::general_overlap(vector<Boid*> boid, double d){

    int N = boid.size();

    for (int i = 0; i < N; i ++) {
        for (int j = i+1 ; j < N; j ++) {
            if(overlap(boid[i], boid[j]) == true || min_dist(boid[i], boid[j], d) == true) return true;
        }
    }

    return false;
}

//--------------------------------------------------------------
void Utils::arrange_boids(vector<Boid*> boid, double Lx, double Ly, double tol, double r, double scale, double d, int species, double mass){

    bool g_ov;
    bool b_ov;
    bool s_ov;

    int N = boid.size();

    g_ov = general_overlap(boid, d);

    printf("Arranging colloids\n");

    while(g_ov == true) {
        // cout << "global overlap" << endl;
        for (int i = 1; i<N; i++) {
            for (int j = 0; j<i; j++) {
                b_ov = overlap(boid[i], boid[j]);
                s_ov = min_dist(boid[i], boid[j], d);
                while (b_ov == true || s_ov == true) {
                    // cout << "overlap\t" << i << "\t" << j << endl;
                    printf(".");
                    boid[i]->verts.clear();
                    boid[i]->segs.clear();
                    boid[i]->setup(species, Lx, Ly, tol, r, scale, mass);
                    b_ov = overlap(boid[i], boid[j]);
                    s_ov = min_dist(boid[i], boid[j], d);
                }
            }
        }
        printf("\n");
        g_ov = general_overlap(boid, d);
    }
}

//--------------------------------------------------------------
