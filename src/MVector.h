//
//  MVector.h
//
//  Created by Martin Zumaya on 1/16/18.
//

#ifndef MVector_h
#define MVector_h

#include <stdio.h>

class MVector {

    public:

    double x;
    double y;

    int size;

    /// OPERATOR OVERLOAD ///
    MVector operator*( const double scalar ) const;
    MVector& operator*=( const MVector& vec );
    MVector& operator*=( const double f );

    MVector operator/( const double scalar ) const;
    MVector& operator/=( const MVector& vec );
    MVector& operator/=( const double f );

    MVector& operator+=( const MVector& vec );
    MVector& operator-=( const MVector& vec );

    MVector operator-() const;
    MVector operator-( const MVector& vec ) const;


    /// VECTOR CONSTRUCTOR ///
    MVector();
    MVector(double x, double y);
    MVector(const MVector &vec);

    /// LINEAR INTERPOLATION 2 VECTORS ///
    MVector getInterpolated(MVector vec, double p);

    /// 2D VECTOR ROTATION BY AN ANGLE  ///
    void rotateRad(double angle, MVector pivot);
    MVector getRotatedRad(double angle, MVector vec);

    /// VECTOR NORMALIZATION ///
    double length();
    double squared_length();
    void normalize();

    /// MODIFY VECTOR COORDINATES ///
    void set(MVector vec);
    void set(double xx, double yy);
    void scale(double sc);

    /// DISTANCE BETWEEN TWO POINTS ///
    double squared_distance(MVector vec);
    double distance(MVector vec);

};

MVector operator*( float f, const MVector& vec );
MVector operator/( float f, const MVector& vec );
MVector operator-( float f, const MVector& vec );

inline MVector MVector::operator-( const MVector& vec ) const {
	return MVector(x-vec.x, y-vec.y);
}

inline MVector operator*( float f, const MVector& vec ) {
    return MVector( f*vec.x, f*vec.y);
}

inline MVector operator/( float f, const MVector& vec ) {
    return MVector( f/vec.x, f/vec.y);
}

inline MVector& MVector::operator*=( const MVector& vec ) {
	x*=vec.x;
	y*=vec.y;
	return *this;
}

inline MVector& MVector::operator/=( const MVector& vec ) {
	x/=vec.x;
	y/=vec.y;
	return *this;
}

inline MVector& MVector::operator*=( const double f ) {
	x*=f;
	y*=f;
	return *this;
}

inline MVector& MVector::operator/=( const double f ) {
	x/=f;
	y/=f;
	return *this;
}

inline MVector MVector::operator*( const double f ) const {
	return MVector(x*f, y*f);
}

inline MVector MVector::operator/( const double f ) const {
	return MVector(x/f, y/f);
}

inline MVector& MVector::operator+=( const MVector& vec ){
    x += vec.x;
    y += vec.y;
    return *this;
}

inline MVector& MVector::operator-=( const MVector& vec ){
    x -= vec.x;
    y -= vec.y;
    return *this;
}

#endif
