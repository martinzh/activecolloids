#ifndef _MDSYS_CPP_
#define _MDSYS_CPP_

#include <cstdlib>

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <fstream>
using std::ifstream;

#include "MVector.h"
#include "Utils.h"
#include "Boid.h"
#include "mdsys.h"

////////////////////////////////////////////////////////////////////////////

MDSystem::MDSystem(int _spc, int _N_colloids, double _box_length, double ratio, double _r, double _visc, double _dt, double _f_prop, double _att_en, double _temp) {


    r          = _r;
    dt         = _dt;
    spc        = _spc;
    visc       = _visc;
    temp       = _temp;
    att_en     = _att_en;
    f_prop     = _f_prop;
    box_length = _box_length;
    N_colloids = _N_colloids;

    /////////////////////////////////////

    box_length_y = (1.0/ratio) * box_length;
    box_length_x = ratio * box_length;

    /////////////////////////////////////

    boid_scale = 2.0;
    mass       = 1.0;
    r_seg      = 0.25;
    tol        = 2.0 * r_seg;
    thrd       = 2.0 * boid_scale;

    /////////////////////////////////////

    // attractive interaction cutoff radius
    r_cutoff_att = 2.5 * r_seg;

    // repulsive interaction cutoff radius
    r_cutoff_rep = pow(2, 1.0/6.0) * r_seg;

    // attractive interaction cutoff radius
    r_cutoff_att_s = 2.5;

    // repulsive interaction cutoff radius
    r_cutoff_rep_s = pow(2, 1.0/6.0);

    // force correction terms (attractive and repulsive)
    double inv_rc_6, inv_rc_d;

    inv_rc_6 = pow(r_seg / r_cutoff_att_s, 6.0);
    inv_rc_d = pow(1.0 / r_cutoff_att_s, 2.0);

    att_corr = inv_rc_d * inv_rc_6 * ( inv_rc_6 - 0.5 );

    inv_rc_6 = pow(r_seg / r_cutoff_rep_s, 6.0);
    inv_rc_d = pow(1.0 / r_cutoff_rep_s, 2.0);

    rep_corr = inv_rc_d * inv_rc_6  * ( inv_rc_6  - 0.5 );

    b_verl = 1.0 / (1.0 + (0.5*visc*dt));

    /////////////////////////////////////

    // // viscosity and time step coefficients
    // t_visc = dt / visc;
    // n_visc = sqrt( 2.0 * (1.0 / visc) * temp * dt);
    //
    // // ang_n_visc = sqrt( 6.0 * (1.0 / visc) * temp * dt);
    // ang_n_visc = sqrt( 2.0 * (1.0 / visc) * temp * dt);

    // viscosity and time step coefficients
    t_visc = 1.0 / visc;
    // n_visc = sqrt( 2.0 * (1.0 / visc) * temp );

    // ang_n_visc = sqrt( 6.0 * (1.0 / visc) * temp );
    // ang_n_visc = sqrt( 2.0 * (1.0 / visc) * temp );

    n_visc = sqrt( 2.0 * visc * temp * dt);
    ang_n_visc = sqrt( 2.0 * visc * temp * dt);

    mass = 1.0;

    // first_int = true;

    /////////////////////////////////////

    for(int id=0; id<N_colloids; id++) {
        Boid* ob = new Boid();
        boid.push_back(ob);
        boid[id]->setup(spc, box_length_x, box_length_y, tol, r, boid_scale, mass);

        MVector* vec = new MVector();
        force.push_back(vec);

        MVector* pr_vec = new MVector();
        prev_force.push_back(pr_vec);
    }

    for(int id=0; id<3*N_colloids; id++) {
        Boid* gh_ob = new Boid();
        gh_boid.push_back(gh_ob);
        gh_boid[id]->setup(spc, box_length_x, box_length_y, tol, r, boid_scale, mass);
    }

    torque      = new double[N_colloids];
    prev_torque = new double[N_colloids];

    gh_flag = new bool[3*N_colloids];

}

////////////////////////////////////////////////////////////////////////////

MDSystem::~MDSystem() {

    for(int i=0; i<N_colloids; i++) {
        delete boid[i];
        delete force[i];
        delete prev_force[i];
    }

    for(int i=0; i<3*N_colloids; i++) {
        delete gh_boid[i];
    }

  // if ( torque!=nullptr ) { delete[] torque; }
  delete[] torque;
  delete[] prev_torque;
  delete[] gh_flag;
}

////////////////////////////////////////////////////////////////////////////

#endif  /* _MDSYS_CPP_ */
